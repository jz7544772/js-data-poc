import { Mapper } from 'js-data';
import { HttpAdapter } from 'js-data-http';

const adapter = new HttpAdapter();

const userService = new Mapper({ name: 'user' });
userService.registerAdapter('http', adapter, { 'default': true });

const postService = new Mapper({ name: 'post', endpoint: 'posts' });
postService.registerAdapter('http', adapter, { 'default': true });

const commentService = new Mapper({ name: 'comment' });
commentService.registerAdapter('http', adapter, { 'default': true });

export default {
	userService, 
	postService,
	commentService
};